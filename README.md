Hi there !
The purpose of this project is to update my portfolio, i wanted to dive into react and as i'm passionned about music, making a spotify clone would be a great challenge right ? :)
So here it is, obviously you'll notice that this is only the landing page with the UI and the musical aspects of the app (Playing music, go previous, go next, etc ...) are not YET implemented, i'll try to update these functionalities later.
The first purpose of this project was to :

Learn about react synthax
Learn a bit of the react context
Try to structure the project to make it readable
Discover the Spotify API
Improve my css skills

To run the project, nothing too fancy, you just have to had a spotify account and :


Go to https://developer.spotify.com/dashboard/login


Click on create an app


Copy the clientID and paste it in the Spotify.js file


If you want more informations check the spotify documentation page : https://developer.spotify.com/documentation/
Credits to CleverProgrammer for making theses app clones, if you want to check them, go to : https://www.youtube.com/c/CleverProgrammer/featured.