import React from 'react'
import '../css/SidebarOption.css'
function SidebarOption({title, Icon}) {
    console.log(title);
    return (
        <div className="sidebarOption">
            {Icon && <Icon className="sidebarOption__icon" />}
            {Icon ? <h4>{title}</h4> : <p>{title}</p>}
           
        </div>
    )
}

export default SidebarOption
