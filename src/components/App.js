import "../css/App.css";
import Login from "./Login";
import React, {useEffect, useState} from "react";
import { getTokenFromUrl } from "../spotify-utils/Spotify";
import SpotifyWebApi from "spotify-web-api-js";
import Player from "./Player";
import { useDataLayerValue } from "../context-API/DataLayer";

const spotify = new SpotifyWebApi();
function App() {


  const [{user,token}, dispatch] = useDataLayerValue();


  //Se lance au lancement de l'application ou à la modification de la variable dans les brackets []
  useEffect(() => {
    const hash = getTokenFromUrl();
    window.location.hash = "";
    const _token = hash.access_token;
    
    //Permet de stocker le token dans le state de l'app
    if (_token){
      dispatch({
        type:"SET_TOKEN",
        token : _token
      })      
      spotify.setAccessToken(_token);

      spotify.getMe().then(user => {
        dispatch({
          type : 'SET_USER',
          user: user
        })
      })
      spotify.getUserPlaylists().then((playlists) => {
        dispatch({
          type : "SET_PLAYLISTS",
          playlists : playlists
        })
      })
      spotify.getPlaylist("37i9dQZEVXcFGbUv7CLkxb").then(response => {
        dispatch({
          type : "SET_DISCOVER_WEEKLY",
          discover_weekly : response
        })
      })
    }
  },[])
  console.log(user);
  console.log('token received', token);
  return (
    <div className="App">
      {
        token ? (
         <Player spotify={spotify}/>
        ) : (
          <Login />
        )
      }
    </div>
  );
}

export default App;
